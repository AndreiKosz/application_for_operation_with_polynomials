/**
 * Created by Admin on 14.03.2017.
 */
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

public class TestOperatii {
    @Test
    public void adunare() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(1, 4);
        Monom m3 = new Monom(0, 5);
        Monom m4 = new Monom(2, 3);
        Monom m5 = new Monom(1, 2);
        Monom m6 = new Monom(0, 6);

        lista.add(m3);
        lista.add(m2);
        lista.add(m1);
        lista2.add(m6);
        lista2.add(m5);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+5.0x^2+6.0x^1+11.0x^0", Operatii.add(p1, p2).outString());
    }
    @Test
    public void scadere() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(1, 4);
        Monom m3 = new Monom(0, 5);
        Monom m4 = new Monom(2, 3);
        Monom m5 = new Monom(1, 2);
        Monom m6 = new Monom(0, 6);

        lista.add(m3);
        lista.add(m2);
        lista.add(m1);
        lista2.add(m6);
        lista2.add(m5);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("-1.0x^2+2.0x^1-1.0x^0",Operatii.sub(p1,p2).outString());
    }
    @Test
    public void mul() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(1, 4);
        Monom m3 = new Monom(0, 5);
        Monom m4 = new Monom(2, 3);
        Monom m5 = new Monom(1, 2);
        Monom m6 = new Monom(0, 6);

        lista.add(m3);
        lista.add(m2);
        lista.add(m1);
        lista2.add(m6);
        lista2.add(m5);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+6.0x^4+16.0x^3+35.0x^2+34.0x^1+30.0x^0",Operatii.mul(p1,p2).outString());
    }
    @Test
    public void intr() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(1, 4);
        Monom m3 = new Monom(0, 5);
        Monom m4 = new Monom(2, 3);
        Monom m5 = new Monom(1, 2);
        Monom m6 = new Monom(0, 6);

        lista.add(m3);
        lista.add(m2);
        lista.add(m1);
        lista2.add(m6);
        lista2.add(m5);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+0.6666666666666666x^3+2.0x^2+5.0x^1",Operatii.intr(p1).outString());
    }
    @Test
    public void deriv() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(1, 4);
        Monom m3 = new Monom(0, 5);
        Monom m4 = new Monom(2, 3);
        Monom m5 = new Monom(1, 2);
        Monom m6 = new Monom(0, 6);

        lista.add(m3);
        lista.add(m2);
        lista.add(m1);
        lista2.add(m6);
        lista2.add(m5);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+4.0x^1+4.0x^0",Operatii.deriv(p1).outString());
    }
}



