/**
 * Created by Admin on 14.03.2017.
 */
/**
 * Created by Admin on 13.03.2017.
 */
import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

public class ControllerMain  {

private Polinom pol,pol2;
private Operatii op=new Operatii();
    public ControllerMain() {
    }

    @FXML
    // The reference of inputText will be injected by the FXML loader
    private TextField input1,input2;

    // The reference of outputText will be injected by the FXML loader
    @FXML
    private TextArea output;
    @FXML
    private Label text1;

    // location and resources will be automatically injected by the FXML loader
    @FXML
    private URL location;

    @FXML
    private ResourceBundle resources;


    public void adunareOut(){
        Parser p1=new Parser(input1.getText());
        Parser p2=new Parser(input2.getText());
        pol=p1.splitPolinom();
        pol2=p2.splitPolinom();
        output.setText(op.add(pol,pol2).outString());
        op.add(pol,pol2).clear();

    }
    public void scadereOut(){
        Parser p1=new Parser(input1.getText());
        Parser p2=new Parser(input2.getText());
        pol=p1.splitPolinom();
        pol2=p2.splitPolinom();
        output.setText(op.sub(pol,pol2).outString());
        op.add(pol,pol2).clear();

    }
    public void mulOut(){
        Parser p1=new Parser(input1.getText());
        Parser p2=new Parser(input2.getText());
        pol=p1.splitPolinom();
        pol2=p2.splitPolinom();
        output.setText(op.mul(pol,pol2).outString());
        op.add(pol,pol2).clear();

    }

    public void derivareOut(){
        Parser p1=new Parser(input1.getText());
        pol=p1.splitPolinom();
        output.setText(op.deriv(pol).outString());
        op.add(pol,pol2).clear();

    }

    public void integrareOut(){
        Parser p1=new Parser(input1.getText());
        pol=p1.splitPolinom();
        output.setText(op.intr(pol).outString());
        op.add(pol,pol2).clear();

    }


    }




