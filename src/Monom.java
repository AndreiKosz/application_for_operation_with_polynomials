import org.jetbrains.annotations.NotNull;

/**
 * Created by Admin on 07.03.2017.
 */
public class Monom implements Comparable<Monom> {

    private double coef;
    private int putere;

    public Monom(int putere,double coef){
        this.putere=putere;
        this.coef=coef;
    }

    public double getCoef() {
        return coef;
    }

    public int getPutere() {
        return putere;
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }


    @Override
    public int compareTo(@NotNull Monom o) {


        return Double.compare(this.putere,o.putere);
    }
}
