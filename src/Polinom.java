import java.util.ArrayList;

/**
 * Created by Admin on 07.03.2017.
 */
public class Polinom {

    private ArrayList<Monom>pol=new ArrayList<Monom>();
    private String str="";

    public Polinom(ArrayList<Monom> m){

        pol=m;
    }

    public Monom getMonom(int i) {
        return pol.get(i);
    }


    public ArrayList<Monom> getPol() {
        return pol;
    }

    public void clear(){
        this.getPol().clear();
    }

    public String outString() {
        for (int i=this.getPol().size()-1;i>=0;i--) {
            if(this.getPol().get(i).getCoef()>=0) {
                str = str+"+"+ this.getPol().get(i).getCoef()+ "x" + "^" + this.getPol().get(i).getPutere();
            }
            else{

                str = str+ this.getPol().get(i).getCoef() + "x" + "^" + this.getPol().get(i).getPutere();
            }

        }
        return str;

    }




    }



