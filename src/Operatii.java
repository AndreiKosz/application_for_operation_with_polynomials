import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Admin on 07.03.2017.
 */
public class Operatii {

    private static Polinom rez;
    private static Monom mon;
    private static Monom mon2;
    private String str = "";
    private static ArrayList<Monom> lista = new ArrayList<>();
    private static ArrayList<Integer> lista2 = new ArrayList<>();

    public Operatii() {

    }

    public static Polinom add(Polinom p1, Polinom p2) {
        for (Monom i : p1.getPol()) {
            mon = new Monom(i.getPutere(), i.getCoef());
            lista.add(mon);
        }
        for (Monom i : p2.getPol()) {
            mon = new Monom(i.getPutere(), i.getCoef());
            lista.add(mon);
        }
        Collections.sort(lista);
        for (int i = 0; i < lista.size() - 1; i++) {

            if (lista.get(i).getPutere() == lista.get(i + 1).getPutere()) {
                lista.get(i + 1).setCoef(lista.get(i).getCoef() + lista.get(i + 1).getCoef());
                lista.remove(i);
                i--;
            }
            }
        return new Polinom(lista);

        }




    public static Polinom mul(Polinom p1, Polinom p2) {

        for (int i = 0; i < p1.getPol().size(); i++) {
            for (int j = 0; j < p2.getPol().size(); j++) {
                mon = new Monom(p1.getMonom(i).getPutere() + p2.getMonom(j).getPutere(), p1.getMonom(i).getCoef() * p2.getMonom(j).getCoef());
                lista.add(mon);
            }
        }
        Collections.sort(lista);
        for (int i = 0; i < lista.size() - 1; i++) {

            if (lista.get(i).getPutere() == lista.get(i + 1).getPutere()) {
                lista.get(i + 1).setCoef(lista.get(i).getCoef() + lista.get(i + 1).getCoef());
                lista.remove(i);
                i--;
            }

        }

        return new Polinom(lista);
    }

    public static Polinom sub(Polinom p1, Polinom p2) {
        for (Monom i : p1.getPol()) {
            mon = new Monom(i.getPutere(), i.getCoef());
            lista.add(mon);
        }
        for (Monom i : p2.getPol()) {
            mon = new Monom(i.getPutere(), i.getCoef());
            lista.add(mon);
        }
        Collections.sort(lista);
        for (int i = 0; i < lista.size() - 1; i++) {

            if (lista.get(i).getPutere() == lista.get(i + 1).getPutere()) {
                lista.get(i + 1).setCoef(lista.get(i).getCoef() - lista.get(i + 1).getCoef());
                lista.remove(i);
                i--;
            }
            else{
                for(Monom j:p2.getPol()){

                    if(lista.get(i).getCoef()==j.getCoef()&&lista.get(i).getPutere()==j.getPutere()){
                        lista.get(i).setCoef(0-lista.get(i).getCoef());
                    }

                    if(lista.get(lista.size()-1).getCoef()==j.getCoef()&&lista.get(lista.size()-1).getPutere()==j.getPutere()&&i==lista.size()-2){
                        //lista.get(lista.size()-1).setCoef(0-lista.get(lista.size()-1).getCoef());

                    }
                }
            }

        }

        return new Polinom(lista);
    }


    public static Polinom deriv(Polinom p1) {

        for (Monom i : p1.getPol()) {
            if (i.getPutere() > 0) {
                mon = new Monom(i.getPutere() - 1, i.getCoef() * i.getPutere());
                lista.add(mon);
            }
        }
        return new Polinom(lista);
    }

    public static Polinom intr(Polinom p1) {
        for (Monom i : p1.getPol()) {
            if (i.getPutere() == 0) {
                mon = new Monom(i.getPutere() + 1, i.getCoef());
                lista.add(mon);
            } else {
                mon = new Monom(i.getPutere() + 1, i.getCoef() / (i.getPutere() + 1));
                lista.add(mon);
            }
        }
        return new Polinom(lista);
    }

}

