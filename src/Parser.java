import java.util.ArrayList;

/**
 * Created by Admin on 10.03.2017.
 */
public class Parser {

    private ArrayList<Monom> monom =new ArrayList<>();
    private String polinom;
    private String[] temp;
    private String[] temp2;
    private String[] temp4;
    private Monom mon;
    private double coef;
    private int putere;
    private String[] single;
    public Parser(String polinom){
        this.polinom=polinom;

    }

    public Polinom splitPolinom(){

        String[] temp = polinom.split("\\+|-");
        String newTemp=new String(polinom.substring(1));
        String[] temp3=newTemp.split("\\+|-");

        String[] semn=polinom.split("[0-9]{1,}x\\^[0-9]");

        if(polinom.length()==4||polinom.length()==5){
            single=polinom.split("x\\^");
            coef = Integer.parseInt(single[0]);
            putere = Integer.parseInt(single[1]);
            mon = new Monom(putere, coef);
            monom.add(mon);

        }
        else {

            if (polinom.charAt(0) != '-') {
                for (int i = temp.length - 1; i >= 0; i--) {
                    temp2 = temp[i].split("x\\^");

                    coef = Integer.parseInt(temp2[0]);
                    putere = Integer.parseInt(temp2[1]);
                    if (semn[i].equals("-")) {
                        mon = new Monom(putere, 0 - coef);
                        monom.add(mon);
                    } else {
                        mon = new Monom(putere, coef);
                        monom.add(mon);
                    }
                }
            } else {
                for (int i = temp3.length - 1; i >= 0; i--) {
                    temp4 = temp3[i].split("x\\^");
                    coef = Integer.parseInt(temp4[0]);
                    putere = Integer.parseInt(temp4[1]);

                    if (semn[i].equals("-")) {
                        mon = new Monom(putere, 0 - coef);
                        monom.add(mon);
                    } else {
                        mon = new Monom(putere, coef);
                        monom.add(mon);
                    }
                }
            }
        }
        return new Polinom(monom);
    }
}
